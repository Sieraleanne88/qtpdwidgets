#-----------------------------------------------------------------------------
#
# vim: syntax=config
#
# Copyright (C) 2012-2021  Florian Pose <fp@igh.de>
#
# This file is part of the QtPdWidgets library.
#
# The QtPdWidgets library is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# The QtPdWidgets library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
# General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with the QtPdWidgets Library. If not, see
# <http://www.gnu.org/licenses/>.
#
#-----------------------------------------------------------------------------

TEMPLATE = lib
TARGET = QtPdWidgets

# good to know: if compiling for android the unix target is selected as well

# On release:
# - Change version in Doxyfile
# - Change version in .spec file
# - Add a NEWS entry
VERSION = 1.3.7
WIN32_LIB_VERSION = 1

greaterThan(QT_MAJOR_VERSION, 4) {
    CONFIG += debug_and_release
    QT += widgets
    !android {
        QT += designer
    }
    DEFINES += PD_QT5 # needed for moc processing of src/WidgetCollection.h
}
else {
    CONFIG += designer release
}


CONFIG += plugin

QT += network xml svg

greaterThan(QT_MAJOR_VERSION, 4) {
QT += quick
}

DEPENDPATH += .
MOC_DIR = .moc
OBJECTS_DIR = .obj

!android {
    QMAKE_CXXFLAGS = -std=gnu++0x
}

CONFIG += c++11

isEmpty(PREFIX) {
    unix:PREFIX = /usr/local
    win32:PREFIX = "c:/msys/1.0/local"
}

LIBEXT=""
unix:!android {
    HARDWARE_PLATFORM = $$system(uname -m)
    contains(HARDWARE_PLATFORM, x86_64) {
        !exists("/etc/debian_version") {
            LIBEXT="64"
        }
    }
}

!isEmpty(EXPATPREFIX) {
    INCLUDEPATH += $${EXPATPREFIX}/include
    LIBS += -L$${EXPATPREFIX}/lib$${LIBEXT}
}

!isEmpty(PDCOMPREFIX) {
    INCLUDEPATH += $${PDCOMPREFIX}/include
    LIBS += -L$${PDCOMPREFIX}/lib$${LIBEXT}
}

LIBS += -lpdcom -lexpat

win32 {
    LIBS += -lwsock32 # for gethostname in Process.cpp
    CONFIG(debug, debug|release): TARGET = $$join(TARGET,,d)
    CONFIG += dll
}

isEmpty(PD_INSTALL_PLUGINS) {
    PD_INSTALL_PLUGINS = $$[QT_INSTALL_PLUGINS]
}

target.path = $${PD_INSTALL_PLUGINS}/designer

isEmpty(NO_DESIGNER){
    INSTALLS += target
}

unix | android { # android just for clarification (see comment above)
    isEmpty(LIBDIR) {
        libraries.path = $${PREFIX}/lib$${LIBEXT}
    }
    else {
        libraries.path = $${LIBDIR}
    }

    libraries.files = $${OUT_PWD}/libQtPdWidgets.so

    INSTALLS += libraries
}

win32 {
    libraries.path = "$${PREFIX}/lib"
    CONFIG(release, debug|release):libraries.files = \
        "release/libQtPdWidgets$${WIN32_LIB_VERSION}.dll.a"
    CONFIG(debug, debug|release):libraries.files = \
        "debug/libdQtPdWidgets$${WIN32_LIB_VERSION}.dll.a"

    dlls.path = "$${PREFIX}/bin"
    CONFIG(release, debug|release):dlls.files = \
        "release/QtPdWidgets$${WIN32_LIB_VERSION}.dll"
    CONFIG(debug, debug|release):dlls.files = \
        "debug/dQtPdWidgets$${WIN32_LIB_VERSION}.dll"

    INSTALLS += dlls libraries
}

inst_headers.path = $${PREFIX}/include/QtPdWidgets

#place files here which are depended on QWidget
QWIDGETDEPHEADERFILES = \
    QtPdWidgets/Bar.h \
    QtPdWidgets/CheckBox.h \
    QtPdWidgets/ClipImage.h \
    QtPdWidgets/CursorEditWidget.h \
    QtPdWidgets/Dial.h \
    QtPdWidgets/Digital.h \
    QtPdWidgets/DoubleSpinBox.h \
    QtPdWidgets/Export.h \
    QtPdWidgets/Graph.h \
    QtPdWidgets/Image.h \
    QtPdWidgets/Led.h \
    QtPdWidgets/MultiLed.h \
    QtPdWidgets/NoPdTouchEdit.h \
    QtPdWidgets/PushButton.h \
    QtPdWidgets/RadioButton.h \
    QtPdWidgets/Rotor.h \
    QtPdWidgets/Scale.h \
    QtPdWidgets/Settings.h \
    QtPdWidgets/SpinBox.h \
    QtPdWidgets/Svg.h \
    QtPdWidgets/TableView.h \
    QtPdWidgets/Tank.h \
    QtPdWidgets/Text.h \
    QtPdWidgets/Time.h \
    QtPdWidgets/TimeScale.h \
    QtPdWidgets/TouchEdit.h \
    QtPdWidgets/TouchEditDialog.h \
    QtPdWidgets/XYGraph.h

inst_headers.files = \
    QtPdWidgets/Message.h \
    QtPdWidgets/MessageModel.h \
    QtPdWidgets/Process.h \
    QtPdWidgets/ScalarSubscriber.h \
    QtPdWidgets/ScalarVariable.h \
    QtPdWidgets/ScalarVariant.h \
    QtPdWidgets/TableColumn.h \
    QtPdWidgets/TableModel.h \
    QtPdWidgets/Translator.h \
    QtPdWidgets/ValueRing.h \
    QtPdWidgets/VectorVariant.h \
    QtPdWidgets/Widget.h

greaterThan(QT_MAJOR_VERSION, 4) {
   inst_headers.files += QtPdWidgets/LiveSvg.h
}

!android {
    inst_headers.files += $${QWIDGETDEPHEADERFILES}
}

INSTALLS += inst_headers

isEmpty(PD_INSTALL_QML) {
    isEmpty($$[QT_INSTALL_QML]) {
        # fallback for older Qt5 versions
        PD_INSTALL_QML = $$[QT_INSTALL_LIBS]/qt5/qml
    }
    else {
        PD_INSTALL_QML = $$[QT_INSTALL_QML]
    }
}
QML_PATH = $${PD_INSTALL_QML}/de/igh/pd

inst_qml.path = $${QML_PATH}
inst_qml.files = \
    qml/de_igh_pdQmlWidgets.pri \
    qml/de_igh_pdQmlWidgets.qrc \
    qml/FontAwesome.otf \
    qml/PdBar.qml \
    qml/PdButton.qml \
    qml/PdCheckBox.qml \
    qml/PdComboBox.qml \
    qml/PdDigital.qml \
    qml/PdImage.qml \
    qml/PdLabel.qml \
    qml/PdSlider.qml \
    qml/PdStatusIndicator.qml \
    qml/PdSwitch.qml \
    qml/PdTimeLabel.qml \
    qml/PdToolButton.qml \
    qml/PdTouchEdit.qml \
    qml/PdTouchEdit.1.1.qml \
    qml/PdTumbler.qml \
    qml/PdVectorIndicator.qml \
    qml/PdVectorLabel.qml \
    qml/qmldir \
    qml/Sprintf.js \
    qml/TouchEditDialog.qml \
    qml/TouchEdit.qml

inst_qml_style.path = $${QML_PATH}/style
inst_qml_style.files = \
    qml/style/qmldir \
    qml/style/Solarized.qml

INSTALLS += inst_qml inst_qml_style

HEADERS += \
    $${inst_headers.files}

!android {
       HEADERS += \
       src/BarSection.h \
       src/BarStack.h
}

# place files here which are depended on QWidget
QWIDGETDEPSOURCES = \
    src/Bar.cpp \
    src/BarSection.cpp \
    src/BarStack.cpp \
    src/CheckBox.cpp \
    src/ClipImage.cpp \
    src/CursorEditWidget.cpp \
    src/Dial.cpp \
    src/Digital.cpp \
    src/DoubleSpinBox.cpp \
    src/Graph.cpp \
    src/Image.cpp \
    src/Led.cpp \
    src/MultiLed.cpp \
    src/NoPdTouchEdit.cpp \
    src/PushButton.cpp \
    src/RadioButton.cpp \
    src/Rotor.cpp \
    src/Scale.cpp \
    src/Settings.cpp \
    src/SpinBox.cpp \
    src/Svg.cpp \
    src/TableView.cpp \
    src/Time.cpp \
    src/TimeScale.cpp \
    src/Tank.cpp \
    src/Text.cpp \
    src/TouchEdit.cpp \
    src/TouchEditDialog.cpp \
    src/XYGraph.cpp

SOURCES += \
    src/Message.cpp \
    src/MessageModel.cpp \
    src/Process.cpp \
    src/ScalarSubscriber.cpp \
    src/ScalarVariant.cpp \
    src/TableColumn.cpp \
    src/TableModel.cpp \
    src/Translator.cpp \
    src/VectorVariant.cpp \
    src/Widget.cpp

greaterThan(QT_MAJOR_VERSION, 4) {
    SOURCES += src/LiveSvg.cpp
}

!android {
    SOURCES += $${QWIDGETDEPSOURCES}
}

!android | isEmpty(DISABLE_PLUGIN) {
    HEADERS += \
        src/Plugin.h \
        src/WidgetCollection.h

    SOURCES += \
        src/Plugin.cpp \
        src/WidgetCollection.cpp
}

RESOURCES = QtPdWidgets.qrc

TRANSLATIONS = \
    QtPdWidgets_de.ts \
    QtPdWidgets_nl.ts

QM_FILES = $$replace(TRANSLATIONS, "ts$", "qm")

CODECFORTR = UTF-8

ADDITIONAL_DISTFILES = \
    AUTHORS \
    COPYING \
    COPYING.LESSER \
    Doxyfile \
    NEWS \
    images/* \
    QtPdWidgets.spec \
    README \
    TODO \
    mydist.sh

DISTFILES += $${ADDITIONAL_DISTFILES}

unix {
    dox.target = dox
    dox.commands = doxygen Doxyfile
    QMAKE_EXTRA_TARGETS += dox

    tags.target = tags
    tags.commands = etags --members $${SOURCES} $${HEADERS}
    QMAKE_EXTRA_TARGETS += tags

    MYDISTFILES = \
        $${ADDITIONAL_DISTFILES} \
        $${HEADERS} \
        $${SOURCES} \
        $${TARGET}.pro \
        $${RESOURCES} \
        $${TRANSLATIONS} \
        $${QM_FILES} \
        test/MainWindow.cpp \
        test/MainWindow.h \
        test/MainWindow.ui \
        test/main.cpp \
        test/test.pro

    mydist.target = mydist
    mydist.commands = ./mydist.sh $${TARGET}-$${VERSION} $${MYDISTFILES}
    QMAKE_EXTRA_TARGETS += mydist
}

#-----------------------------------------------------------------------------
