/*****************************************************************************
 *
 * Copyright (C) 2009 - 2020  Wilhelm Hagemeister <hm@igh.de>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef QMLBUDDY_H
#define QMLBUDDY_H

#include <QObject>
#include <QtPdWidgets/Process.h>
#include <QTimer>
#include <QVariant>
#include <QFileSystemWatcher>

#include <QtPdWidgets/ScalarVariant.h>


class QmlBuddy:
public QObject
{
  Q_OBJECT
  Q_PROPERTY(bool connected READ getConnected NOTIFY connectionStatusChanged)
 public:

  QmlBuddy(QObject * = Q_NULLPTR);
  ~QmlBuddy();

  bool getConnected() { return isConnected; };
    Pd::Process process;
  private slots:
    void processConnected();
    void processDisconnected();
    void processError();
    void tryConnect();
 signals:
   void connectionStatusChanged(bool);
 private:
    bool isConnected;
    QTimer connectTimer;
    void connectToProcess();
    void updateConnection();
};

/****************************************************************************/

#endif
