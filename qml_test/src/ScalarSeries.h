/*****************************************************************************
 *
 * Copyright (C) 2018  Wilhelm Hagemeister<hm@igh.de>, Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_SCALARSERIES_H
#define PD_SCALARSERIES_H

#include <QObject>
#include <QtCharts/QAbstractSeries>

QT_CHARTS_USE_NAMESPACE

#include <QtPdWidgets/ScalarSubscriber.h>
#include <QtPdWidgets/Process.h>
#include <QtPdWidgets/ValueRing.h>

using Pd::ValueRing;

/****************************************************************************/

/** Scalar Series to be used in QML applications
    which is aware of the process to detect connections/or reconnect.
    It supplies a series function to be used in QML ChartViews
 */

class ScalarSeries:
  public QObject, public Pd::ScalarSubscriber
{
    Q_OBJECT
    Q_PROPERTY(QString path READ getPath WRITE setPath NOTIFY pathChanged)
    Q_PROPERTY(double sampleTime READ getSampleTime WRITE setSampleTime NOTIFY sampleTimeChanged)
    Q_PROPERTY(double scale READ getScale WRITE setScale NOTIFY scaleChanged)
    Q_PROPERTY(double timeRange READ getTimeRange WRITE setTimeRange NOTIFY timeRangeChanged)
    Q_PROPERTY(Pd::Process *process READ getProcess WRITE setProcess NOTIFY processChanged)
    public:
        ScalarSeries();
        virtual ~ScalarSeries();

	QString getPath() const { return path; };
	double getSampleTime() { return sampleTime; };
	double getScale() { return _scale; };
        void clearData(); // pure-virtual from ScalarSubscriber
        Q_INVOKABLE bool hasData() const { return dataPresent; };
	Pd::Process *getProcess() const { return process; };

	void setPath(QString &);
	void setSampleTime(double);
	void setScale(double);
        double getTimeRange() const;
        void setTimeRange(double);

	void setProcess(Pd::Process *);

	public slots:
	  void update(QAbstractSeries *series); /**< send data to the Chart */

    private:
	Pd::Process *process;
        QString path;
	double sampleTime;
	double _scale;
        double timeRange; /**< Time range. */
        bool dataPresent; /**< There is a process value to display. */
	ValueRing<double> values; /**< Ring buffer with time/value
				     pairs. */

	void updateConnection(); /**< (re)connects to variable */

        void notify(PdCom::Variable *); // pure-virtual from PdCom::Subscriber

    private slots:
        void processConnected();
	void processDisconnected();
	void processError();

    signals:
	void pathChanged(QString &);
	void sampleTimeChanged(double);
	void scaleChanged(double);
	void processChanged();
        void timeRangeChanged();
};


#endif
