/*****************************************************************************
 *
 * Copyright (C) 2009 - 2020  Wilhelm Hagemeister <hm@igh.de>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

/****************************************************************************
**
** PD-QML Demo-Applikation (requires a EtherLab process running at localhost:
**                          /usr/bin/pdserv-example-st)
**
****************************************************************************/


#include <QtQml>
#include <QObject>
#include <QDebug>
#include <QNetworkInterface>
#include <QQuickStyle>
#include <QFont>

#include <QtWidgets/QApplication>
#include <QtQuick/QQuickView>

#include "QmlBuddy.h"
#include <QtPdWidgets/ScalarVariant.h>
#include <QtPdWidgets/VectorVariant.h>
#include "ScalarSeries.h"
#include <QtPdWidgets/LiveSvg.h>


#define literal(val) #val
#define stringify(val) literal(val)


/*--------------------------------------------------------------------------*/

int main(int argc, char *argv[])
{
  QApplication::setAttribute(Qt::AA_EnableHighDpiScaling); 

  //qputenv("QT_SCALE_FACTOR","1.25");
  //qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));

  QApplication app(argc, argv);

  //für Settings
  app.setOrganizationName("IgH");
  app.setOrganizationDomain("igh.de");
  app.setApplicationName("QMLWIDGETSDEMO");
  app.setApplicationVersion(QString("%1").arg(stringify(HGVERSION)));
  QQmlApplicationEngine* engine = new QQmlApplicationEngine(&app);

  QmlBuddy* buddy = new QmlBuddy(&app);

  //expose cpp objects to qml
  engine->rootContext()->setContextProperty("Buddy",buddy);
  engine->rootContext()->setContextProperty("pdProcess",&buddy->process);

  //register cpp classes with the qml system
  //allows qml to create instances of this classes
  qmlRegisterType<Pd::ScalarVariant>("de.igh.pd",1,0,"PdScalar");
  qmlRegisterType<Pd::VectorVariant>("de.igh.pd",1,0,"PdVector");
  qmlRegisterType<ScalarSeries>("de.igh.pd",1,0,"PdSeriesData");
  qmlRegisterType<Pd::LiveSvg>("de.igh.svg", 1, 0, "LiveSvg");

  QQuickStyle::setStyle("Material");
  //if main goes in to the resources file
  engine->load(QUrl("qrc:/qml/main.qml"));

  //if main is access as a file
  //engine->load(QUrl("qml/main.qml"));

  qDebug() << engine->importPathList();

  if (engine->rootObjects().isEmpty()) {
    return -1;
  }
  return app.exec();
}
