/*****************************************************************************
 *
 * $Id$
 *
 * Copyright (C) 2018  Wilhelm Hagemeister <hm@igh.de>, with code from
 *                     Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

/****************************************************************************/

#include "ScalarSeries.h"
#include <QtCharts/QXYSeries>

QT_CHARTS_USE_NAMESPACE

#define DEFAULT_TIMERANGE  10.0


/** Constructor.
 */
ScalarSeries::ScalarSeries():
  process(0),
  path(""),
  sampleTime(0),
  _scale(1.0),
  timeRange(DEFAULT_TIMERANGE),
  dataPresent(false)
{
}

/****************************************************************************/

/** Destructor.
 */
ScalarSeries::~ScalarSeries()
{
}

/****************************************************************************/

void ScalarSeries::updateConnection(){
    if(process && process->isConnected()) {
      //qDebug() << "try connect: " << path << "with sampletime: " << sampleTime;
      PdCom::Variable *pv = process->findVariable(path);
      setVariable(pv,sampleTime,_scale);
    }

}

/****************************************************************************/

void ScalarSeries::setProcess(Pd::Process *value){

  //  qDebug() << "set Process" << value;
  if(value != process) {
    if(process) { //detach from "old" process
      clearVariable();
      QObject::disconnect(process,0,0,0);
    }
    if(value) {
      process = value;
      QObject::connect(process, SIGNAL(connected()), this, SLOT(processConnected()));
      QObject::connect(process, SIGNAL(disconnected()), this, SLOT(processDisconnected()));
      QObject::connect(process, SIGNAL(error()), this, SLOT(processError()));
      updateConnection();
    }
    emit processChanged();
  }
}

/****************************************************************************/

void ScalarSeries::processConnected()
{
  updateConnection();
}

/****************************************************************************/

void ScalarSeries::processDisconnected()
{
  clearData();
}

/****************************************************************************/

void ScalarSeries::processError()
{
  clearData();
}

/****************************************************************************/

void ScalarSeries::setPath(QString &value){
  if(value != path) {
    path = value;
    updateConnection();
    emit pathChanged(path);
  }
}

/****************************************************************************/

void ScalarSeries::setSampleTime(double value){
  if(value != sampleTime) {
    sampleTime = value;
    updateConnection();
    emit sampleTimeChanged(sampleTime);
  }
}

/****************************************************************************/
void ScalarSeries::setScale(double value){
  if(value != _scale) {
    _scale = value;
    updateConnection();
    emit scaleChanged(_scale);
  }
}

/****************************************************************************/

void ScalarSeries::setTimeRange(double range)
{
    values.setRange(range + 0.5); /* hold some more values than necessary for
                                     synchronisation reasons. */
    emit timeRangeChanged();
}


double ScalarSeries::getTimeRange() const
{
    return timeRange;
}


/****************************************************************************/

void ScalarSeries::clearData()
{
  values.clear();
    dataPresent = false;
}


/****************************************************************************/

/** This virtual method is called by the ProcessVariable, if its value
 * changes.
 */
void ScalarSeries::notify(
        PdCom::Variable *pv
        )
{
    double value;
    PdCom::Time time;

    pv->getValue(&value, 1, &scale);
    time = pv->getMTime();

    values.append(time, value);
}

/****************************************************************************/
//see: /vol/opt/Qt5.10-android/Examples/Qt-5.10.1/charts/qmloscilloscope

void ScalarSeries::update(QAbstractSeries *series){ 

  if (series && values.getLength() > 1) {
        QXYSeries *xySeries = static_cast<QXYSeries *>(series);

	QVector<QPointF> points(values.getLength());

	double toffset = values[values.getLength()-1].first;

	for (uint i = 0; i < values.getLength(); i++) {
	  ValueRing<double>::TimeValuePair value = values[i];
	  points[i].setX(value.first - PdCom::Time(toffset));
	  points[i].setY(value.second);

	}
	xySeries->replace(points);
  }
}
/****************************************************************************/
