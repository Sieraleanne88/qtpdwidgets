/*****************************************************************************
 *
 * Copyright (C) 2009 - 2020  Wilhelm Hagemeister <hm@igh.de>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include <QDebug>
#include <QSettings>
#include <QDir> 
#include <QProcess>
#include <QNetworkInterface>
#include <QUrl>

#include "QmlBuddy.h"


QmlBuddy::QmlBuddy(QObject *parent):
  QObject(parent)
{

  QSettings settings;

  //signal and slots
  connect(&process, SIGNAL(connected()), this, SLOT(processConnected()));
  connect(&process, SIGNAL(disconnected()), this, SLOT(processDisconnected()));
  connect(&process, SIGNAL(error()), this, SLOT(processError()));
  connectToProcess();

  connectTimer.setSingleShot(false);
  connectTimer.setInterval(2000);
  connect(&connectTimer, SIGNAL(timeout()),this, SLOT(tryConnect()));

}

/****************************************************************************/

QmlBuddy::~QmlBuddy()
{
  qDebug() << "Destroy qmlBuddy";
  process.disconnectFromHost();
}

/****************************************************************************/

void QmlBuddy::processConnected()
{
  updateConnection();
}

/****************************************************************************/

void QmlBuddy::processDisconnected()
{
  updateConnection();
  connectTimer.start();
}

/****************************************************************************/

void QmlBuddy::processError()
{
  updateConnection();
  connectTimer.start();
}

/****************************************************************************/

void QmlBuddy::tryConnect()
{
  connectTimer.stop();
  connectToProcess();
}

/****************************************************************************/

void QmlBuddy::connectToProcess()
{
  QSettings settings;
  process.connectToHost("localhost",2345);
  updateConnection();
}

/****************************************************************************/

void QmlBuddy::updateConnection()
{
  QString info;

  QString host = process.getPeerName();

  switch (process.getConnectionState()) {
  case Pd::Process::Disconnected:
    info = tr("Not connected.");
    isConnected = false;
    break;
  case Pd::Process::Connecting:
    info = tr("Connecting to %1...").arg(host);
    isConnected = false;
    break;
  case Pd::Process::Connected:
    info = tr("Connected to %1.").arg(host);
    isConnected = true;
    break;
  case Pd::Process::ConnectError:
    info = tr("Connection failed to %1.").arg(host);
    isConnected = false;

    break;
  case Pd::Process::ConnectedError:
    info = tr("Connection aborted.");
    isConnected = false;
    break;
  }

  emit connectionStatusChanged(isConnected);
  //qDebug() << info;
}

