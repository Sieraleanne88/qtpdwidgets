import QtQuick 2.7         
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.1

import de.igh.pd 1.0

import "Sprintf.js" as Str

Label {
    id:control
    property alias process: scalar.process
    property alias path: scalar.path
    property alias scale: scalar.scale
    property alias sampleTime:scalar.sampleTime
    property string suffix:""
    property var format:Locale.LongFormat 

    property alias value:scalar.value

    //FIXME only Time implemented yet
    enum Mode {  
        Time, 
        DateLocal, 
        DateTimeLocal,
        DateUTC,
        DateTimeUTC
    }

    //change this if mode is fully implemented
    property int mode:PdTimeLabel.Mode.Time

    PdScalar {
        id:scalar
        onValueChanged:{
            switch(control.mode) {
            case PdTimeLabel.Mode.Time:
                var sec = value % 60;
                var min = ((value % 3600)-sec)/60;
                var h = (value - sec - min*60)/3600;
                control.text = Str.sprintf("%02d:%02d:%02d",h,min,sec);
                break;
	    case PdTimeLabel.Mode.DateLocal:
		var date = new Date(value * 1000);
		control.text = date.toLocaleDateString(Qt.locale("de_DE"),format);
		break;
	    case PdTimeLabel.Mode.DateTimeLocal:
		var date = new Date(value * 1000);
		control.text = date.toLocaleString(Qt.locale("de_DE"),format);
		break;
	    case PdTimeLabel.Mode.DateTimeUTC:
		var date = new Date(value * 1000);
		//var dateUTC = new Date(date.getTime() - date.getTimezoneOffset()*60000);
		control.text = date.toUTCString() //dateUTC.toLocaleString(Qt.locale("de_DE"),format);
		break;
            default:
                control.text = "not implemented yet";
                break;
            }
        }
    }
}
