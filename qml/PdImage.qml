import QtQuick 2.7         
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.1

import de.igh.pd 1.0

Image{
    id:control
    property var hash:[]
    property alias process: scalar.process
    property alias path: scalar.path
    property alias sampleTime:scalar.sampleTime
    property alias value:scalar.value	
    PdScalar {
        id:scalar
        onValueChanged:{
            if(hash[value] != undefined) {
                control.source = hash[value]
            } else {
                control.source = ""
            }
        }
    }
}
