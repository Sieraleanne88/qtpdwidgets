import QtQuick 2.7         
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.1
import QtQuick.Extras 1.4

import de.igh.pd 1.0

StatusIndicator {
    id: indicator
    property alias process: scalar.process
    property alias path: scalar.path
    property alias sampleTime: scalar.sampleTime
    property alias value: scalar.value
    property bool invert:false
    property bool blinking:false
    property bool isOn: ((scalar.value != 0) != invert) 
    opacity:scalar.connected?1:0.3
    //im Gegensatz zu active welches beim blinken wegfällt
    PdScalar {
        id:scalar
        onValueChanged: {
            if(value != invert) {
                if(blinking) {
                    timer.running = true
                } else {
                    indicator.active = true
                }
            } else {
                timer.running = false
                indicator.active = false
            }
        }
    }
    Timer {
        id:timer
        interval: 500;
        repeat: true;
        onTriggered: {
            indicator.active = (!indicator.active)
        }
    }
}
