import QtQuick 2.7         
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.1

import de.igh.pd 1.0

Label {
    id: control
    property alias decimals:dialog.decimals
    property alias suffix:dialog.suffix
    property alias lowerLimit:dialog.lowerLimit
    property alias upperLimit:dialog.upperLimit
    property alias process: scalar.process
    property alias path: scalar.path
    property alias scale: scalar.scale
    property alias offset: scalar.offset
    property string oldText: ""
    property alias value:scalar.value
    enabled:scalar.connected
    onDecimalsChanged:{
        scalar.update()
    }
    onSuffixChanged: {
        scalar.update()
    }

    /**
       this signal is emited if the value is modified by the Dialog.
       It is not emited if the value is modified from other msr connections
    */

    signal accepted()

    TouchEditDialog {
        id:dialog
        onAccepted: {
            scalar.value = value
	    control.accepted()
        }
    }
    PdScalar {
        id:scalar
        function update() {
            control.text = value.toFixed(control.decimals)+control.suffix
        }
        onValueChanged: {
            update()
        }
    }

    background: Rectangle {
        implicitWidth: 100
        implicitHeight: 30
        color:Material.background
        border.color: parent.enabled ? Material.accent : Material.primary
        MouseArea {
            anchors.fill: parent
            onClicked: {         
                dialog.value = scalar.value
                dialog.open() 
            }
        }
    }

    horizontalAlignment: TextInput.AlignRight
    padding:5
}
