import QtQuick 2.7         
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.1

import de.igh.pd 1.0

/* Example Model with indexRole
ListModel {
    id:modeModel
    ListElement {key: "None"; value:0}
    ListElement {key: "a few"; value:4}
    ListElement {key: "many"; value:8} 
    ListElement {key: "all"; value:10} 
}
*/

/* Example Model without indexRole
ListModel {
    id:modeModel
    ListElement {key: "None"}
    ListElement {key: "a few"}
    ListElement {key: "many"} 
    ListElement {key: "all"} 
}
*/

ComboBox {
    id: control
    property alias process: scalar.process
    property alias path: scalar.path
    property alias value: scalar.value
    enabled:scalar.connected
    editable:false  //FIXME how to make this readonly?
    property string indexRole:""  /*like the textRole the value out of the 
				    indexRole is written to the realtime process if the model 
				    has a valid index key. Otherwise the modelIndex is written
				    to the process.*/
    function update(v) {
	if(model === 'undefined' || model.count <= 0) {
	    currentIndex = -1;
	    return;
	}
	if(indexRole.length > 0) {
	    for(var i = 0;i<control.model.count;i++) {
		if(scalar.value == model.get(i)[control.indexRole]) {
		    currentIndex = i;
		    break;
		}
	    }
	} else {  //indexRole == ''
	    if(v >= 0 && v < model.count) {
		currentIndex = v;
	    } else {
		currentIndex = -1;
		return
	    }
	}
    }

    onIndexRoleChanged: update(scalar.value)
    onTextRoleChanged: update(scalar.value)
    onModelChanged: update(scalar.value) 
    
    PdScalar {
        id:scalar
	onValueChanged: control.update(value)
    }  //PdScalar
    
    onActivated: {
	// index
	if(indexRole.length > 0) {
	    scalar.value = model.get(index)[indexRole]
	} else {
	    scalar.value = index
	}
    }
}
