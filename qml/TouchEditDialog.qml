import QtQuick 2.9           
import QtQuick.Layouts 1.3   
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.1

import "Sprintf.js" as Str

//code follows CursorEditWidget/TouchEditDialog in qtPdWidgets

Dialog {
    id:dialog
    anchors.centerIn: Overlay.overlay
    /*
    parent: ApplicationWindow.overlay
    x: Math.round((parent.width - width) / 2)
    y: Math.round((parent.height - height) / 2)
    */
    modal:true
    property double value:0
    property int decimals:2
    property int digPos:0
    property string suffix:""
    property double lowerLimit:-Number.MAX_VALUE
    property double upperLimit:Number.MAX_VALUE
/*    property alias title : titleLabel.text
    header: Label {
        id:titleLabel
        visible:(text.length > 0)? true:false
    }
*/
    FontLoader {
        //source: "qrc:/de/igh/PdQmlWidgets/FontAwesome.otf"
        source: "FontAwesome.otf"
    }
    onValueChanged: { updateValueString() }
    onSuffixChanged: { updateValueString() }
    onLowerLimitChanged: {
        if (value < lowerLimit) {
            value = lowerLimit;
        }
    }
    onUpperLimitChanged: {
        if (value > upperLimit) {
            value = upperLimit;
        }
    }    
    onDecimalsChanged: { 
        decimals = Math.max(decimals,0);
        updateValueString();
    }
    function setValue(v) {
        if (v != value) {
            digPos = 0;
            value = v;
        }
    }

    // -----------------------------------------------------------------
    function isCharNumber(c){
        return c >= '0' && c <= '9';
    }
    // -----------------------------------------------------------------
    function updateValueString() {

        var width = digPos + decimals + 1;

        if (decimals > 0) {
            width++; // with dot
        }

        var fmt = "%0" + width + "." +decimals + "f";

        //console.log("fmt: "+fmt)
        var valueStr = Str.sprintf(fmt, value);

        //var valueStr = value.toFixed(decimals);


        if (valueStr.length > 0) {

            var pos, digCount = 0;
            var html = "";

            for (pos = valueStr.length - 1; pos >= 0; pos--) {
                //console.log("pos: "+pos)
                if (isCharNumber(valueStr.charAt(pos))) {
                    //console.log("is int: "+valueStr.charAt(pos))

                    if (digPos + decimals == digCount) {
                        html = "<span style=\""+
                        "color: blue; "+
                        "text-decoration: underline;"+
                        "\">" + valueStr.charAt(pos) + "</span>" + html;
                    } else {
                        html = valueStr.charAt(pos) + html;
                    }

                    digCount++;
                } else {
                    html = valueStr.charAt(pos) + html;
                }
            }

            html = "<html><head><meta http-equiv=\"Content-Type\" "+
            "content=\"text/html; charset=utf-8\"></head><body>"+
            "<div align=\"center\" style=\""+
            "color: white; "+
            "font-size: 24pt;"+
            "\">" + html + suffix + "</div></body></html>";
            //console.log("html: "+html)
            valueLabel.text = html;
        }

    } 
    // -----------------------------------------------------------------
    function setEditDigit(dig) {
        if (dig < -decimals) {
            dig = -decimals;
        }

        if (upperLimit != Number.MAX_VALUE && lowerLimit != -Number.MAX_VALUE) {
            var emax = Math.max(
            Math.floor(Math.log(Math.abs(upperLimit))/Math.log(10)),
            Math.floor(Math.log(Math.abs(lowerLimit))/Math.log(10)));
            if (dig > emax) {
                dig = emax;
            }
        }

        if (dig != digPos) {
            digPos = dig;
            //console.log("digpos: "+digPos)
            updateValueString();
        }
    }

    // -----------------------------------------------------------------
    function digitUp() {
        var digitValue = Math.pow(10, digPos);
        var eps = 0.5 * Math.pow(10, -decimals - digPos);
        var r = Math.floor(value / digitValue + eps) * digitValue;
        value = r + digitValue;
        if (value > upperLimit) {
            value = upperLimit;
        }
    }

    // -----------------------------------------------------------------
    function digitDown() {
        var digitValue = Math.pow(10, digPos);
        var eps = 0.5 * Math.pow(10, -decimals - digPos);
        var r = Math.ceil(value / digitValue - eps) * digitValue;
        value = r - digitValue;
        if (value < lowerLimit) {
            value = lowerLimit;
        }
    }


    // -----------------------------------------------------------------
    function setZero() {
        if (lowerLimit > 0.0) {
            value = lowerLimit;
        }
        else if (upperLimit < 0.0) {
            value = upperLimit;
        }
        else {
            value = 0.0;
        }
    }
    ColumnLayout {
        Label {

            background:Rectangle {
                color:"lightGray" //Material.primary
                radius:4
            }

            id:valueLabel
            padding:20
            text:"0.0"
            //font.pixelSize:40
            textFormat: Text.RichText
            horizontalAlignment: Qt.AlignHCenter
            verticalAlignment: Qt.AlignVCenter
            Layout.fillWidth:true
        }
        //Button Grid
        GridLayout {
            columns:3
            columnSpacing:10
            rowSpacing:0
            Rectangle { //empty
                width:1 //FIXME must be of size to be in the list
                height:1
                color:Material.background
            }
            Button {
                id:up
                font.family: "FontAwesome"
                font.pixelSize: 20
                text: "\uf062"
                implicitHeight:70
                implicitWidth:120
                autoRepeat:true
                onClicked:{ dialog.digitUp() }
            }
            Rectangle { //empty
                width:1
                height:1
                color:Material.background
                
            }
            Button {
                id:left
                font.family: "FontAwesome"
                font.pixelSize: 20
                text: "\uf060"
                implicitHeight:up.implicitHeight
                implicitWidth:up.implicitWidth
                onClicked:{dialog.setEditDigit(dialog.digPos+1)}
            }
            Button {
                id:zero
                text:"0"
                font.pixelSize: 30
                implicitHeight:up.implicitHeight
                implicitWidth:up.implicitWidth
                onClicked:{ dialog.setZero() }
            }
            Button {
                id:right
                font.family: "FontAwesome"
                font.pixelSize: 20
                text: "\uf061"
                implicitHeight:up.implicitHeight
                implicitWidth:up.implicitWidth
                onClicked:{dialog.setEditDigit(dialog.digPos-1)}
            }
            Button {
                id:cancel
                text:"Cancel"
                implicitHeight:up.implicitHeight
                implicitWidth:up.implicitWidth
                onClicked: { dialog.reject() }
            }
            Button {
                id:down
                font.family: "FontAwesome"
                font.pixelSize: 20
                text: "\uf063"
                implicitHeight:up.implicitHeight
                implicitWidth:up.implicitWidth
                autoRepeat:true
                onClicked:{ dialog.digitDown() }
            }
            Button {
                id:ok
                text:"Ok"
                implicitHeight:up.implicitHeight
                implicitWidth:up.implicitWidth
                onClicked: { dialog.accept() }
            }

        }
    }
    Component.onCompleted:{
        updateValueString()        
    }
} //Dialog
