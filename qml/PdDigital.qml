import QtQuick 2.7         
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.1

import de.igh.pd 1.0

Label {
    id:control
    property alias process: vector.process
    property alias path: vector.path
    property alias scale: vector.scale
    property alias offset: vector.offset
    property alias sampleTime:vector.sampleTime
    property string suffix:""
    property string prefix:""
    property int decimals:2
    property alias value:vector.value
    property alias connected:vector.connected
    enabled:vector.connected

    property int index:-1 //if index is positive only the value of that index
                          //is chown (regardless off the mode)
    property var indices : [] //if Mode = Selection only elements with indexes in this vector are shown
    
    enum Mode {  //for vector elements
        All,     //show all elements
        Limits,  //show the max and minimum
        Mean,    //show mean and std
        Set,     //show a set of distinct values (makes only sense for integers)
	Selection //shows the indices which are listed in the indices vector
    }
    property int mode:PdDigital.Mode.All

    PdVector {
        id:vector
    }

    //Helper functions
    function mean(data){
        var sum = data.reduce(function(sum, value){
            return sum + value;
        }, 0);

        var avg = sum / data.length;
        return avg;
    }

    function std(values){
        var avg = mean(values);
        
        var squareDiffs = values.map(function(value){
            var diff = value - avg;
            var sqrDiff = diff * diff;
            return sqrDiff;
        });
        
        var avgSquareDiff = mean(squareDiffs);

        var stdDev = Math.sqrt(avgSquareDiff);
        return stdDev;
    }
    
    //http://jkorpela.fi/chars/spaces.html
    function norm(t) {
        if(t[0] != '-') {
            return '\u2004'+t;
        } else {
            return t;
        }
    }

    text:{
        var t = "";
        if(value.length == 1) { //scalar
            t = norm(value[0].toFixed(control.decimals));
        } else  {
            if (index > -1) { //auch scalar
                t = norm(value[Math.min(index,value.length-1)].toFixed(control.decimals));
            } else {                //vector
                switch(mode) {
                case PdDigital.Mode.All: 
                    for(var i=0;i<value.length;i++) {
                        t+= norm(value[i].toFixed(control.decimals));
                        if(i != value.length -1) {
                            t+=", ";
                        }
                    }
                    break;
                case PdDigital.Mode.Selection:
		    for(var i=0;i<indices.length;i++) {
                        t+= norm(value[indices[i]].toFixed(control.decimals));
                        if(i != indices.length -1) {
			    t+=", ";
                        }
		    }
                    break;
                case PdDigital.Mode.Limits: 
                    t = norm(Math.min.apply(null,value).toFixed(control.decimals))+' ... '+
			norm(Math.max.apply(null,value).toFixed(control.decimals));
                    break;

                case PdDigital.Mode.Mean:
                    t = "μ: " + norm(mean(value).toFixed(control.decimals))+" (σ: "+
			norm(std(value).toFixed(control.decimals))+")";
                    break;

                case PdDigital.Mode.Set:
                    var set = [];
                    var hit;
                    //FIXME geht sicher performanter
                    t = "{";
                    for(var i=0;i<value.length;i++) {
                        hit = false;
                        for(var j=0;j<set.length;j++) {
                            if (value[i] == set[j]) {
                                hit = true;
                                break;
                            }
                        }

                        if(hit == false) {
                            set.push(value[i]);
                        }
                    }
                    set.sort(function(a, b){return a - b});
                    for(var i=0;i<set.length;i++) {
                        t+= norm(set[i].toFixed(control.decimals));
                        if(i != set.length -1) {
                            t+=", ";
                        }
                    }
                    t+= "}";
                    break;
                }
            }
        }   					    
        t = control.prefix + t + control.suffix;
        return t;
    }
}
