import QtQuick 2.7         
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.1

import de.igh.pd 1.0

//FIXME: Slider does not react on enabled
//TODO: update value as slider moves
Slider {
    id: control
    property alias process: scalar.process
    property alias path: scalar.path

    PdScalar {
        id:scalar
        onValueChanged: {
            if(!control.pressed) { //do not refresh if control is pressed
                control.value = value
            }
        }
    }
    //only write the value at release
    onPressedChanged:{
        if(!pressed) {
            scalar.value = value
        }
    }
}
