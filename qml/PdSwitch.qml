import QtQuick 2.7         
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.1

import de.igh.pd 1.0

Switch {
    id: control
    property alias process: scalar.process
    property alias path: scalar.path
    property bool invert:false
    PdScalar {
        id:scalar
    }
    checked: ((scalar.value != 0) != invert)
    onClicked: scalar.value = (control.checked != invert)
}
