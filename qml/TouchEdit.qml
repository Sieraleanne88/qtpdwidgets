import QtQuick 2.7         
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.1

import de.igh.pd 1.0

Label {
    id: control
    property alias decimals:dialog.decimals
    property alias suffix:dialog.suffix
    property alias lowerLimit:dialog.lowerLimit
    property alias upperLimit:dialog.upperLimit
    property double value:0.0
    text: value.toFixed(decimals)+suffix
    /*
    onValueChanged: {
      text = value.toFixed(decimals)+suffix
    }
    */
    /**
       this signal is emited if the value is modified by the Dialog.
       It is not emited if the value is modified from other msr connections
    */

    signal accepted()
    TouchEditDialog {
        id:dialog
        onAccepted: {
            control.value = value
	    control.accepted()
        }
    }

    background: Rectangle {
        implicitWidth: 100
        implicitHeight: 30
        color:Material.background
        border.color: parent.enabled ? Material.accent : "transparent" //"#21be2b"
        MouseArea {
            anchors.fill: parent
            onClicked: {         
                dialog.value = control.value
                dialog.open() 
            }
        }
    }

    horizontalAlignment: TextInput.AlignRight
    padding:5
}
