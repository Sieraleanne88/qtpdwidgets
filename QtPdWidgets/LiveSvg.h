/****************************************************************************
**
**
****************************************************************************/

#ifndef PD_LIVESVG_H
#define PD_LIVESVG_H

#include <QString>
#include <QSvgRenderer>
#include <QSize>
#include <QMouseEvent>
#include <QSlider>
#include <QDomDocument>
#include <QTimer>
#include <QPixmap>
#include <QSizeF>
#include <QPointF>
#include <QtQuick/QQuickPaintedItem>

namespace Pd {

class LiveSvg : public QQuickPaintedItem
{
  Q_OBJECT
  Q_PROPERTY(QRectF viewBox READ getViewBox NOTIFY viewBoxChanged)
  Q_PROPERTY(QString source READ getSource WRITE setSource NOTIFY sourceChanged) //Image
  Q_PROPERTY(bool invert READ getInvert WRITE setInvert NOTIFY invertedChanged)
public:
    LiveSvg(QQuickItem *parent = 0); 
    void paint(QPainter *painter);
    Q_INVOKABLE QVariant getOverlayElements() { return overlayElements; };
    Q_INVOKABLE QString getSource() { return source; };
    bool getInvert() { return invert; };
    void setInvert(bool);
    void setSource(const QString &s);
    void clearSource();

    QRectF getViewBox() { return viewBox;};
    //public slots:

  signals:
  void scaleChanged(QSizeF scale);
  void viewBoxChanged();
  void sourceChanged();
  void invertedChanged();
private:
    QDomDocument m_svgdoc;
    QSvgRenderer m_renderer;
    QRectF viewBox;
    QPixmap backgroundPixmap; /**< Pixmap that stores the background. */
    QList<QVariant> overlayElements;
    QString source;
    bool empty; //* no pixmal
    bool invert;
    QDomElement findLayer(const QString &layerName,const QDomElement& parent);
    void findElementsWithAttribute(const QDomElement& elem, const QString& attr, QList<QDomElement> &foundElements);
    void getOverlayRects(const QDomElement& parent);
    void getTransformations(const QDomNode& elem, QPointF& offset);

    void printElements( QList<QDomElement> elements);
    void printAttributes(QDomElement elem);
    void updateBackground();
    void scaleQmlChildren(double,double,double, double);

};

 /****************************************************************************/

}  //namespace

#endif
