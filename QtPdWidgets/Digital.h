/*****************************************************************************
 *
 * Copyright (C) 2009 - 2019  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_DIGITAL_H
#define PD_DIGITAL_H

#include <QFrame>

#include "ScalarSubscriber.h"
#include "Widget.h"

#include <memory>

namespace Pd {

/****************************************************************************/

/** Base functionality for digital displays.
 */
class Q_DECL_EXPORT Digital:
    public QFrame, public ScalarSubscriber, Widget
{
    Q_OBJECT

    Q_ENUMS(TimeDisplay)

    Q_PROPERTY(Qt::Alignment alignment
            READ getAlignment WRITE setAlignment RESET resetAlignment)
    Q_PROPERTY(quint32 decimals
            READ getDecimals WRITE setDecimals RESET resetDecimals)
    Q_PROPERTY(QString suffix
            READ getSuffix WRITE setSuffix RESET resetSuffix)
    Q_PROPERTY(TimeDisplay timeDisplay
            READ getTimeDisplay WRITE setTimeDisplay RESET resetTimeDisplay)
    Q_PROPERTY(int base
            READ getBase WRITE setBase RESET resetBase)

    public:
        Digital(QWidget *parent = 0);
        virtual ~Digital();

        void clearData(); // pure-virtual from ScalarSubscriber

        double getValue() const;

        Qt::Alignment getAlignment() const;
        void setAlignment(Qt::Alignment);
        void resetAlignment();

        quint32 getDecimals() const;
        void setDecimals(quint32);
        void resetDecimals();

        const QString &getSuffix() const;
        void setSuffix(const QString &);
        void resetSuffix();

        enum TimeDisplay {
            None,
            Seconds,
            Minutes,
            Hours
        };
        TimeDisplay getTimeDisplay() const;
        void setTimeDisplay(TimeDisplay);
        void resetTimeDisplay();

        int getBase() const;
        void setBase(int);
        void resetBase();

        const QString &getValueString() const;

        QSize sizeHint() const;

    protected:
        bool event(QEvent *);
        void paintEvent(QPaintEvent *);

    private:
        struct Impl;
        std::unique_ptr<Impl> impl;

        void notify(PdCom::Variable *); // pure-virtual from PdCom::Subscriber

    private slots:
        void redrawEvent();
};

/****************************************************************************/

} // namespace

#endif
