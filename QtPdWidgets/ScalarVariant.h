/*****************************************************************************
 *
 * Copyright (C) 2018  Wilhelm Hagemeister<hm@igh.de>, Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_SCALARVARIANT_H
#define PD_SCALARVARIANT_H

#include <QObject>
#include <QVariant>


#include "ScalarSubscriber.h"
#include "Process.h"

namespace Pd {

/****************************************************************************/

/** Scalar Variant to be used in QML applications
    which is aware of the process to detect connections/or reconnect
 */

class ScalarVariant:
  public QObject, public Pd::ScalarSubscriber
{
    Q_OBJECT
    Q_PROPERTY(QString path READ getPath WRITE setPath NOTIFY pathChanged)
    Q_PROPERTY(double sampleTime READ getSampleTime WRITE setSampleTime NOTIFY sampleTimeChanged)
    Q_PROPERTY(double scale READ getScale WRITE setScale NOTIFY scaleChanged)
    Q_PROPERTY(double offset READ getOffset WRITE setOffset NOTIFY offsetChanged)
    /* indicates that the process is connected and data is transfered */
    Q_PROPERTY(bool connected READ getDataPresent NOTIFY dataPresentChanged) 
    Q_PROPERTY(Pd::Process *process READ getProcess WRITE setProcess NOTIFY processChanged)
    Q_PROPERTY(QVariant value READ getValue WRITE setValue NOTIFY valueChanged)
    Q_PROPERTY(QVariant mtime READ getMTimeToDouble NOTIFY valueUpdated)
    public:
        ScalarVariant();
        virtual ~ScalarVariant();

	QString getPath() const { return path; };
	double getSampleTime() { return sampleTime; };
	double getScale() { return _scale; };
	double getOffset() { return _offset; };
	bool getDataPresent() { return dataPresent; };

        void clearData(); // pure-virtual from ScalarSubscriber
        Q_INVOKABLE bool hasData() const { return dataPresent; };
	Pd::Process *getProcess() const { return process; };

	void setPath(QString &);
	void setSampleTime(double);
	void setScale(double);
	void setOffset(double);
	void setProcess(Pd::Process *);
        QVariant getValue() const { return value; };
	Q_INVOKABLE void setValue(QVariant);
        PdCom::Time getMTime() const { return mTime; };
        double getMTimeToDouble(){ return (double)mTime; };
        Q_INVOKABLE void inc();

    private:
	Pd::Process *process;
        QVariant value; /**< Current value. */
        QString path;
	double sampleTime;
	double _scale;
	double _offset;
        PdCom::Time mTime; /**< Modification Time of Current value. */
        bool dataPresent; /**< There is a process value to display. */
	void updateConnection(); /**< (re)connects to variable */

        void notify(PdCom::Variable *); // pure-virtual from PdCom::Subscriber

    private slots:
        void processConnected();
	void processDisconnected();
	void processError();

    signals:
        void valueChanged(QVariant&); /**< Emitted, when the value changes, or the
					 variable is disconnected. */
	void valueUpdated(double mtime); /**< Emitted also when value does not change but 
					    got an update from the msr process */
	void pathChanged(QString &);
	void sampleTimeChanged(double);
	void scaleChanged(double);
	void offsetChanged(double);
	void processChanged();
	void dataPresentChanged(bool);
};

/****************************************************************************/

} // namespace


#endif
