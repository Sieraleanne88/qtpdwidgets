/*****************************************************************************
 *
 * Copyright (C) 2018  Wilhelm Hagemeister <hm@igh.de>, with code from
 *                     Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

/****************************************************************************/

#include "QtPdWidgets/VectorVariant.h"

using Pd::VectorVariant;

/** Constructor.
 */

VectorVariant::VectorVariant(QObject *parent):
  QObject(parent),
  variable(NULL),
  process(0),
  path(""),
  sampleTime(0),
  _scale(1.0),
  _offset(0.0),
  dataPresent(false)
{
  items.clear();
}

/****************************************************************************/

/** Destructor.
 */
VectorVariant::~VectorVariant()
{
  clearVariable();
}


/****************************************************************************/

/** Unsubscribe from a Variable.
 */
void VectorVariant::clearVariable()
{
    if (variable) {
        variable->unsubscribe(this);
        variable = NULL;
        clearData();
    }
}

/****************************************************************************/

/** Subscribes to a ProcessVariable.
 */
void VectorVariant::setVariable(
        PdCom::Variable *pv,
        double sampleTime,
        double gain,
	double offset
        )
{
    clearVariable();

    if (!pv) {
        return;
    }

    scale.gain = gain;
    scale.offset = offset;

    try {
        pv->subscribe(this, sampleTime);
    } catch (PdCom::Exception &e) {
        qCritical() << QString("Failed to subscribe to variable"
                " \"%1\" with sample time %2: %3")
            .arg(QString(pv->path.c_str()))
            .arg(sampleTime)
            .arg(e.what());
        return;
    }

    variable = pv;
    if (!sampleTime)
        pv->poll(this); // poll once to get initial value
}

/****************************************************************************/

void VectorVariant::updateConnection(){
  //FIXME test for vector here 
    if(process && process->isConnected()) {
      //qDebug() << "try connect: " << path << "with sampletime: " << sampleTime;
      PdCom::Variable *pv = process->findVariable(path);
      setVariable(pv,sampleTime,_scale,_offset);
    }

}

/****************************************************************************/

void VectorVariant::setProcess(Pd::Process *value){

  //  qDebug() << "set Process" << value;
  if(value != process) {
    if(process) { //detach from "old" process
      clearVariable();
      QObject::disconnect(process,0,0,0);
    }
    if(value) {
      process = value;
      QObject::connect(process, SIGNAL(connected()), this, SLOT(processConnected()));
      QObject::connect(process, SIGNAL(disconnected()), this, SLOT(processDisconnected()));
      QObject::connect(process, SIGNAL(error()), this, SLOT(processError()));
      updateConnection();
    }
    emit processChanged();
  }
}

/****************************************************************************/

void VectorVariant::processConnected()
{
  updateConnection();
}

/****************************************************************************/

void VectorVariant::processDisconnected()
{
  clearData();
}

/****************************************************************************/

void VectorVariant::processError()
{
  clearData();
}

/****************************************************************************/

void VectorVariant::setPath(QString &value){
  if(value != path) {
    path = value;
    updateConnection();
    emit pathChanged(path);
  }
}

/****************************************************************************/

void VectorVariant::setSampleTime(double value){
  if(value != sampleTime) {
    sampleTime = value;
    updateConnection();
    emit sampleTimeChanged(sampleTime);
  }
}

/****************************************************************************/
void VectorVariant::setScale(double value){
  if(value != _scale) {
    _scale = value;
    updateConnection();
    emit scaleChanged(_scale);
  }
}

/****************************************************************************/
void VectorVariant::setOffset(double value){
  if(value != _offset) {
    _offset = value;
    updateConnection();
    emit offsetChanged(_offset);
  }
}

/****************************************************************************/

void VectorVariant::clearData()
{
  if(dataPresent) {
    items.clear();
    dataPresent = false;
    emit dataPresentChanged(dataPresent);
    emit valueChanged();
  }
}

/****************************************************************************/

void VectorVariant::setValueAsString(const QString &value){

  if(!dataPresent)
    return;

  PdCom::Variable *pv = getProcessVariable();

  if(!pv)
    return;

  int pvDim = pv->dimension.getElementCount();
  int cnt = pvDim;
  
  //a String can be stored in an uint8 array as well
  
  if (pv->type == PdCom::Data::uint8_T) {
    QString s = value;
    QByteArray data;
    do { //the string with the trailing 0 must not be longer than the array.
      //We need to chop the string and not the byteArray because there can be
      //an utf8 char at the end which is longer than one byte
      data = s.toUtf8();
      s.chop(1);
    } while(data.size() > cnt-1);
    data.append('\0');
    //qDebug() << "to data: " << data;
    pv->setValue((signed char*) data.data(),data.size());
  }
}

/****************************************************************************/

QString VectorVariant::getValueAsString() {
  if(items.size() > 0) {
    QByteArray data;
    for(int i = 0;i < items.size();i++) {
      data.append(items[i].toInt());
    }
    //qDebug() << "from data: " << data;
    data.append('\0');
    return  QString::fromUtf8(data);
  } else {
    return "";
  }
}

/****************************************************************************/

void VectorVariant::setValue(const QVariant &value){

  if(!dataPresent)
    return;

  PdCom::Variable *pv = getProcessVariable();

  if(!pv)
    return;

  int pvDim = pv->dimension.getElementCount();
  int cnt = pvDim;

  bool isNumber = false;
  value.toDouble(&isNumber);
  
  bool isList = value.canConvert<QVariantList>(); //see assistant about canConvert() and convert()

  bool isString = value.canConvert<QString>();
  
  //a String can be stored in an uint8 array as well
  
  if(isString && !isNumber && !isList) {
    if (pv->type == PdCom::Data::uint8_T) {
      QString s = value.toString();
      QByteArray data;
      do { //the string with the trailing 0 must not be longer than the array
	   //we need to chop the string and not the byteArray because there can be
	   //an utf8 char at the end which is longer than one byte
	data = s.toUtf8();
	s.chop(1);
      } while(data.size() > cnt-1);
      pv->setValue((signed char*) data.data(),data.size());
      
    }
    qWarning() << "type error: string is supplied to PdVector but variable is not uint8";
    return;  //can't be anything else
  }

  if(isList) {
    cnt = qMin(value.toList().count(),pvDim);
  }

  switch (pv->type) {
  case PdCom::Data::bool_T:
  case PdCom::Data::uint8_T:
  case PdCom::Data::uint16_T:
  case PdCom::Data::uint32_T:
  case PdCom::Data::uint64_T:{
    uint64_t data[pvDim];
    if(isList) {
      for(int i = 0;i<cnt;i++) {
	data[i] = value.toList().at(i).toULongLong(&isNumber);
	if(!isNumber) goto out;
      }
      pv->setValue(data,cnt,&scale);
    } else { //it might be a scalar, then set all items to the same value
      for(int i = 0;i<cnt;i++) {
	data[i] = value.toULongLong(&isNumber);
	if(!isNumber) goto out;
      }
      pv->setValue(data,cnt,&scale);
    }
  }
    break;
  case PdCom::Data::sint8_T:
  case PdCom::Data::sint16_T:
  case PdCom::Data::sint32_T:
  case PdCom::Data::sint64_T:{
    int64_t data[pvDim];
    if(isList) {
      for(int i = 0;i<cnt;i++) {
	data[i] = value.toList().at(i).toLongLong(&isNumber);
	if(!isNumber) goto out;

      }
      pv->setValue(data,cnt,&scale);
    } else {
      for(int i = 0;i<cnt;i++) {
	data[i] = value.toLongLong(&isNumber);
	if(!isNumber) goto out;

      }
      pv->setValue(data,cnt,&scale);
    }
  }
    break;
  case PdCom::Data::single_T:
  case PdCom::Data::double_T:{
    double data[cnt];
    if(isList) {
      for(int i = 0;i<cnt;i++) {
	data[i] = value.toList().at(i).toDouble(&isNumber);
	if(!isNumber) goto out;
      }
      pv->setValue(data,cnt,&scale);
    } else {
      for(int i = 0;i<cnt;i++) {
	data[i] = value.toDouble(&isNumber);
	if(!isNumber) goto out;
      }
      pv->setValue(data,cnt,&scale);
    }
  }
    break;
  default:
    qWarning() << "unknown datatype";
    break;
  }
  return;
out:
  qWarning() << "type error: list is supplied to PdVector but element are not numeric";
  return;
}

/****************************************************************************/

/** Notification for variable deletion.
 *
 * This virtual function is called by the Variable, when it is about to be
 * destroyed.
 */
void VectorVariant::notifyDelete(PdCom::Variable *)
{
    variable = NULL;
    clearData();
}

/****************************************************************************/

/** This virtual method is called by the ProcessVariable, if its value
 * changes.
 */
void VectorVariant::notify(
        PdCom::Variable *pv
        )
{

  int pvDim = pv->dimension.getElementCount();

  items.clear();

  switch (pv->type) {
  case PdCom::Data::bool_T:
  case PdCom::Data::uint8_T:
  case PdCom::Data::uint16_T:
  case PdCom::Data::uint32_T:
  case PdCom::Data::uint64_T:{
    uint64_t uInt[pvDim];
    pv->getValue(uInt, pvDim, &scale);

    for(int i=0;i<pvDim;i++) {
      items.append((qint64)uInt[i]);
    }
    dataPresent = true;
    emit dataPresentChanged(dataPresent);
    emit valueChanged();
  }
    break;
  case PdCom::Data::sint8_T:
  case PdCom::Data::sint16_T:
  case PdCom::Data::sint32_T:
  case PdCom::Data::sint64_T:{
    int64_t sInt[pvDim];
    pv->getValue(sInt,pvDim, &scale);
    for(int i=0;i<pvDim;i++) {
      items.append((qint64)sInt[i]);
    }
    dataPresent = true;
    emit dataPresentChanged(dataPresent);
    emit valueChanged();
  }
    break;
  case PdCom::Data::single_T:
  case PdCom::Data::double_T:{
    double d[pvDim];
    pv->getValue(d,pvDim, &scale);

    for(int i=0;i<pvDim;i++) {
      items.append(d[i]);
    }
    dataPresent = true;
    emit dataPresentChanged(dataPresent);
    emit valueChanged();
  }
    break;
  default:
    qWarning() << "unknown datatype";
    break;
  }

  mTime = pv->getMTime();

  emit valueUpdated(mTime);
}

/****************************************************************************/
