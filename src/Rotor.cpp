/*****************************************************************************
 *
 * Copyright (C) 2009 - 2019  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "QtPdWidgets/Rotor.h"

using Pd::Rotor;

#include <QtGui>
#include <QSvgRenderer>
#include <QTimer>

#include <pdcom/Subscriber.h>

/****************************************************************************/

#define DEFAULT_ANGLE 0.0

#define PERIOD 0.040 // 25 Hz

/****************************************************************************/

struct Rotor::Impl:
    public PdCom::Subscriber
{
    Impl(Rotor *);
    ~Impl();

    Rotor * const rotor;

    QString backgroundPath;
    QString rotorPath;
    QString foregroundPath;
    QPointF rotorCenter;
    double globalAngle;

    PdCom::Variable *speedVariable;
    PdCom::Variable::Scale speedScale; /**< Scale vector. */
    double speedFilterConstant;
    bool speedDataPresent;
    double speedValue;

    QTimer timer;
    double rotorAngle;

    double scale;
    QPointF rotationOffset;

    QSvgRenderer backgroundRenderer;
    bool backgroundLoaded;
    QSvgRenderer rotorRenderer;
    bool rotorLoaded;
    QSvgRenderer foregroundRenderer;
    bool foregroundLoaded;

    void notify(PdCom::Variable *);
    void notifyDelete(PdCom::Variable *);
    void updateScale();
};

/****************************************************************************/

Rotor::Rotor(
        QWidget *parent
        ):
    QFrame(parent),
    impl(std::unique_ptr<Impl>(new Impl(this)))
{
}

/****************************************************************************/

Rotor::~Rotor()
{
}

/****************************************************************************/

/** Gives a hint aboute the optimal size.
 */
QSize Rotor::sizeHint() const
{
    return QSize(100, 100);
}

/****************************************************************************/

const QString &Rotor::getBackground() const
{
    return impl->backgroundPath;
}

/****************************************************************************/

void Rotor::setBackground(const QString &path)
{
    if (impl->backgroundPath == path) {
        return;
    }

    impl->backgroundPath = path;

    if (path.isEmpty()) {
        impl->backgroundRenderer.load(QByteArray());
        impl->backgroundLoaded = false;
    }
    else {
        impl->backgroundLoaded = impl->backgroundRenderer.load(path);
    }

    impl->updateScale();
}

/****************************************************************************/

void Rotor::resetBackground()
{
    setBackground(QString());
}

/****************************************************************************/

const QString &Rotor::getRotor() const
{
    return impl->rotorPath;
}

/****************************************************************************/

void Rotor::setRotor(const QString &path)
{
    if (impl->rotorPath == path) {
        return;
    }

    impl->rotorPath = path;

    if (path.isEmpty()) {
        impl->rotorRenderer.load(QByteArray());
        impl->rotorLoaded = false;
    }
    else {
        impl->rotorLoaded = impl->rotorRenderer.load(path);
    }

    impl->updateScale();
}

/****************************************************************************/

void Rotor::resetRotor()
{
    setRotor(QString());
}

/****************************************************************************/

const QString &Rotor::getForeground() const
{
    return impl->foregroundPath;
}

/****************************************************************************/

void Rotor::setForeground(const QString &path)
{
    if (impl->foregroundPath == path) {
        return;
    }

    impl->foregroundPath = path;

    if (path.isEmpty()) {
        impl->foregroundRenderer.load(QByteArray());
        impl->foregroundLoaded = false;
    }
    else {
        impl->foregroundLoaded = impl->foregroundRenderer.load(path);
    }

    update();
}

/****************************************************************************/

void Rotor::resetForeground()
{
    setForeground(QString());
}

/****************************************************************************/

QPointF Rotor::getRotorCenter() const
{
    return impl->rotorCenter;
}

/****************************************************************************/

void Rotor::setRotorCenter(QPointF p)
{
    if (impl->rotorCenter == p) {
        return;
    }

    impl->rotorCenter = p;
    update();
}

/****************************************************************************/

void Rotor::resetRotorCenter()
{
    setRotorCenter(QPointF(0.0, 0.0));
}

/****************************************************************************/

double Rotor::getGlobalAngle() const
{
    return impl->globalAngle;
}

/****************************************************************************/

void Rotor::setGlobalAngle(double a)
{
    if (impl->globalAngle == a) {
        return;
    }

    impl->globalAngle = a;
    impl->updateScale();
}

/****************************************************************************/

void Rotor::resetGlobalAngle()
{
    setGlobalAngle(DEFAULT_ANGLE);
}

/****************************************************************************/

/** Subscribes to a ProcessVariable.
 */
void Rotor::setSpeedVariable(
        PdCom::Variable *pv,
        double sampleTime,
        double gain,
        double offset,
        double tau
        )
{
    clearSpeedVariable();

    if (!pv) {
        return;
    }

    impl->speedScale.gain = gain;
    impl->speedScale.offset = offset;

    if (tau > 0.0 && sampleTime > 0.0) {
        impl->speedFilterConstant = sampleTime / tau;
    } else {
        impl->speedFilterConstant = 0.0;
    }

    try {
        pv->subscribe(impl.get(), sampleTime);
    } catch (PdCom::Exception &e) {
        qCritical() << QString("Failed to subscribe to variable"
                " \"%1\" with sample time %2: %3")
            .arg(QString(pv->path.c_str()))
            .arg(sampleTime)
            .arg(e.what());
        return;
    }

    impl->speedVariable = pv;

    if (!sampleTime) {
        pv->poll(impl.get()); // poll once to get initial value
    }
}

/****************************************************************************/

/** Unsubscribe from a Variable.
 */
void Rotor::clearSpeedVariable()
{
    if (impl->speedVariable) {
        impl->speedVariable->unsubscribe(impl.get());
        impl->speedVariable = NULL;
        update();
    }
}

/****************************************************************************/

/** Event function.
 */
bool Rotor::event(
        QEvent *event /**< Event flags. */
        )
{
    return QFrame::event(event);
}

/****************************************************************************/

void Rotor::resizeEvent(QResizeEvent *)
{
    impl->updateScale();
}

/****************************************************************************/

void Rotor::paintEvent(QPaintEvent *event)
{
    QFrame::paintEvent(event);

    QPainter p(this);

    p.setRenderHint(QPainter::Antialiasing);

    if (impl->scale == 0.0) {
        return;
    }

    p.scale(impl->scale, impl->scale);
    p.translate(-impl->rotationOffset);
    p.rotate(impl->globalAngle);

    QSize size;

    if (impl->backgroundPath.isEmpty()) {
        size = impl->rotorRenderer.defaultSize();
    }
    else {
        size = impl->backgroundRenderer.defaultSize();
    }

    QRect renderRect(QPoint(), size);
    impl->backgroundRenderer.render(&p, renderRect);

    p.save();
    p.translate(impl->rotorCenter);
    p.rotate(-impl->rotorAngle);
    p.translate(-impl->rotorCenter);
    impl->rotorRenderer.render(&p, renderRect);
    p.restore();

    impl->foregroundRenderer.render(&p, renderRect);
}

/****************************************************************************/

void Rotor::timeout()
{
    if (impl->speedDataPresent && impl->speedValue != 0.0) {
        impl->rotorAngle += impl->speedValue * PERIOD;
        update();
    }
}

/*****************************************************************************
 * Implementation
 ****************************************************************************/

Rotor::Impl::Impl(Rotor *rotor):
    rotor(rotor),
    rotorCenter(0.0, 0.0),
    globalAngle(DEFAULT_ANGLE),
    speedVariable(NULL),
    speedDataPresent(false),
    rotorAngle(0.0),
    scale(0.0),
    backgroundRenderer(rotor),
    backgroundLoaded(false),
    rotorRenderer(rotor),
    rotorLoaded(false),
    foregroundRenderer(rotor),
    foregroundLoaded(false)
{
    connect(&timer, SIGNAL(timeout()), rotor, SLOT(timeout()));

    timer.setSingleShot(false);
    timer.start(PERIOD * 1000);

    updateScale();
}

/****************************************************************************/

Rotor::Impl::~Impl()
{
    timer.stop();
    rotor->clearSpeedVariable();
}

/****************************************************************************/

/** This virtual method is called by the ProcessVariable, if its value
 * changes.
 */
void Rotor::Impl::notify(PdCom::Variable *pv)
{
    if (pv == speedVariable) {
        double v;
        pv->getValue(&v, 1, &speedScale);

        if (speedDataPresent) {
            double newValue;

            if (speedFilterConstant > 0.0) {
                newValue =
                    speedFilterConstant * (v - speedValue) + speedValue;
            } else {
                newValue = v;
            }

            speedValue = newValue;
        } else {
            speedValue = v; // bypass filter
            speedDataPresent = true;
        }
    }
}

/****************************************************************************/

/** Notification for variable deletion.
 *
 * This virtual function is called by the Variable, when it is about to be
 * destroyed.
 */
void Rotor::Impl::notifyDelete(PdCom::Variable *pv)
{
    if (pv == speedVariable) {
        speedVariable = NULL;
        speedDataPresent = false;
        rotor->update();
    }
}

/****************************************************************************/

void Rotor::Impl::updateScale()
{
    /* workaround for designer not accepting QString properties as resources.
     * try to reload SVG data on next opportunity. */

    if (!backgroundPath.isEmpty() && !backgroundLoaded) {
        backgroundLoaded = backgroundRenderer.load(backgroundPath);
    }
    if (!rotorPath.isEmpty() && !rotorLoaded) {
        rotorLoaded = rotorRenderer.load(rotorPath);
    }
    if (!foregroundPath.isEmpty() && !foregroundLoaded) {
        foregroundLoaded = foregroundRenderer.load(foregroundPath);
    }

    QSize size;

    if (backgroundPath.isEmpty()) {
        size = rotorRenderer.defaultSize();
    }
    else {
        size = backgroundRenderer.defaultSize();
    }

    QRect renderRect(QPoint(), size);
    QMatrix rot;

    rot.rotate(globalAngle);
    QRect rotBound = rot.mapRect(renderRect);

    if (rotBound.width() > 0) {
        scale = 1.0 * rotor->contentsRect().width() / rotBound.width();
        rotationOffset = rotBound.topLeft();
    }
    else {
        scale = 0.0;
    }

    rotor->update();
}

/****************************************************************************/
