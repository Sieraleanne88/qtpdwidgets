/*****************************************************************************
 *
 * Copyright (C) 2009 - 2012  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include <QDebug>
#include <QString>
#include <QStyle>
#include <QtGui>

#include "QtPdWidgets/Svg.h"

using Pd::Svg;

/****************************************************************************/
/** Constructor.
 */
Svg::Svg(
        QWidget *parent /**< Parent widget. */
        ):
    QFrame(parent),
    angle(0.0),
    imageLoaded(false)
{

}

/****************************************************************************/

/** Destructor.
 */
Svg::~Svg()
{
    // Nothing to do here.
}

/****************************************************************************/

void Svg::setSvgPath(const QString &path)
{
    if (svgPath == path) {
        return;
    }

    svgPath = path;

    if (svgPath.isEmpty()) {
        imageRenderer.load(QByteArray());
        imageLoaded = false;
        elementList = QList<ElementList>();

    } else {
        imageLoaded = imageRenderer.load(svgPath);
        loadFile();
        printList();
        update();

    }

}

/****************************************************************************/

/** Method for debug only.
*/
void Svg::printList()
{
    for (int i = 0; i < elementList.length(); i++) {
        qDebug() << elementList[i].id;
    }
}

/****************************************************************************/

/** Resets the #svgPath.
 */
void Svg::resetSvgPath()
{
    setSvgPath(QString());
}

/****************************************************************************/

void Svg::loadFile()
{
    QFile file(svgPath);
    domDoc.setContent(&file);
    file.close();

    QDomElement rootElem = domDoc.documentElement();
    parseElement(rootElem.childNodes());

}

/****************************************************************************/

void Svg::parseElement(QDomNodeList nds)
{
    if (nds.isEmpty()) {
        return;
    }

    for (int i = 0; i < nds.length(); i++) {
        ElementList entry;
        QDomElement elem = nds.at(i).toElement();

        entry.element = elem;
        entry.id = elem.attribute("id");
        elementList.append(entry);

        parseElement(elem.childNodes());
    }
}

/****************************************************************************/

/** Sets the #angle to use.
 */
void Svg::setAngle(qreal a)
{
    if (a != angle) {
        angle = a;
        update();
    }
}

/****************************************************************************/

/** Resets the #angle.
 */
void Svg::resetAngle()
{
    setAngle(0.0);
}

/****************************************************************************/

void Svg::setIdList(const QStringList &list) {
    QStringList existIds;
    reqIds = list;
}

/****************************************************************************/

QStringList Svg::getIdList() const {
    QStringList ids;
    for (int i = 0; i < elementList.length(); i++) {
        ids.append(elementList.at(i).id);
    }
    return ids;
}

/****************************************************************************/

bool Svg::existId(QString id) const {
    for (int i = 0; i < elementList.length(); i++) {
        if (elementList.at(i).id.compare(id) == 0) {
            return true;
        }
    }
    return false;
}

/****************************************************************************/

void Svg::paintEvent(QPaintEvent *event)
{
    QFrame::paintEvent(event);

    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing);

    QSize size = contentsRect().size();
    QRect renderRect(QPoint(), size);

    imageRenderer.render(&p, renderRect);


}

/****************************************************************************/
