/*****************************************************************************
 *
 * Copyright (C) 2018  Wilhelm Hagemeister <hm@igh.de>, with code from
 *                     Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

/****************************************************************************/

#include "QtPdWidgets/ScalarVariant.h"

using Pd::ScalarVariant;

/** Constructor.
 */
ScalarVariant::ScalarVariant():
  process(0),
  value(0),
  path(""),
  sampleTime(0),
  _scale(1.0),
  _offset(0.0),
  dataPresent(false)
{
}

/****************************************************************************/

/** Destructor.
 */
ScalarVariant::~ScalarVariant()
{
}

/****************************************************************************/

void ScalarVariant::updateConnection(){
  //FIXME test for scalar here 
    if(process && process->isConnected()) {
      //qDebug() << "try connect: " << path << "with sampletime: " << sampleTime;
      PdCom::Variable *pv = process->findVariable(path);
      setVariable(pv,sampleTime,_scale,_offset);
    }

}

/****************************************************************************/

void ScalarVariant::setProcess(Pd::Process *value){

  //  qDebug() << "set Process" << value;
  if(value != process) {
    if(process) { //detach from "old" process
      clearVariable();
      QObject::disconnect(process,0,0,0);
    }
    if(value) {
      process = value;
      QObject::connect(process, SIGNAL(connected()), this, SLOT(processConnected()));
      QObject::connect(process, SIGNAL(disconnected()), this, SLOT(processDisconnected()));
      QObject::connect(process, SIGNAL(error()), this, SLOT(processError()));
      updateConnection();
    }
    emit processChanged();
  }
}

/****************************************************************************/

void ScalarVariant::processConnected()
{
  updateConnection();
}

/****************************************************************************/

void ScalarVariant::processDisconnected()
{
  clearData();
}

/****************************************************************************/

void ScalarVariant::processError()
{
  clearData();
}

/****************************************************************************/

void ScalarVariant::setPath(QString &value){
  if(value != path) {
    path = value;
    updateConnection();
    emit pathChanged(path);
  }
}

/****************************************************************************/

void ScalarVariant::setSampleTime(double value){
  if(value != sampleTime) {
    sampleTime = value;
    updateConnection();
    emit sampleTimeChanged(sampleTime);
  }
}

/****************************************************************************/
void ScalarVariant::setScale(double value){
  if(value != _scale) {
    _scale = value;
    updateConnection();
    emit scaleChanged(_scale);
  }
}

/****************************************************************************/
void ScalarVariant::setOffset(double value){
  if(value != _offset) {
    _offset = value;
    updateConnection();
    emit offsetChanged(_offset);
  }
}


/****************************************************************************/

void ScalarVariant::clearData()
{
    value = 0;
    dataPresent = false;
    emit dataPresentChanged(dataPresent);
    emit valueChanged(value);
}

/****************************************************************************/

void ScalarVariant::setValue(QVariant value)
{
    //qDebug() <<"setValue called with: " << value;

    if (!dataPresent) {
        return;
    }

    switch (getProcessVariable()->type) {
        case PdCom::Data::bool_T:
        case PdCom::Data::uint8_T:
        case PdCom::Data::uint16_T:
        case PdCom::Data::uint32_T:
        case PdCom::Data::uint64_T:
            writeValue((uint64_t)value.toULongLong());
            break;
        case PdCom::Data::sint8_T:
        case PdCom::Data::sint16_T:
        case PdCom::Data::sint32_T:
        case PdCom::Data::sint64_T:
            writeValue((int64_t)value.toLongLong());
            break;
        case PdCom::Data::single_T:
        case PdCom::Data::double_T:
            writeValue(value.toDouble());
            break;
        default:
            qWarning() << "unknown datatype";
            break;
    }
}

/****************************************************************************/

/** Increments the current #value and writes it to the process.
 *
 * This does \a not update #value directly.
 */
void ScalarVariant::inc()
{
  writeValue(value.toInt() + 1);
}

/****************************************************************************/

/** This virtual method is called by the ProcessVariable, if its value
 * changes.
 */
void ScalarVariant::notify(
        PdCom::Variable *pv
        )
{
    switch (pv->type) {
    case PdCom::Data::bool_T:
    case PdCom::Data::uint8_T:
    case PdCom::Data::uint16_T:
    case PdCom::Data::uint32_T:
    case PdCom::Data::uint64_T:{
      uint64_t uInt;
      pv->getValue(&uInt, 1, &scale);
      if (uInt != value.toULongLong() || !dataPresent) {
        value = (quint64)uInt;
        dataPresent = true;
	emit dataPresentChanged(dataPresent);
        emit valueChanged(value);
      }

    }
      break;
    case PdCom::Data::sint8_T:
    case PdCom::Data::sint16_T:
    case PdCom::Data::sint32_T:
    case PdCom::Data::sint64_T:{
      int64_t sInt;
      pv->getValue(&sInt, 1, &scale);
      if (sInt != value.toLongLong() || !dataPresent) {
        value = (qint64)sInt;
        dataPresent = true;
	emit dataPresentChanged(dataPresent);
	emit valueChanged(value);
      }
    }
      break;
    case PdCom::Data::single_T:
    case PdCom::Data::double_T:{
      double d;
      pv->getValue(&d, 1, &scale);
      if (d != value.toDouble() || !dataPresent) {
        value = d;
        dataPresent = true;
	emit dataPresentChanged(dataPresent);
        emit valueChanged(value);
      }
    }
      break;
    default:
      qWarning() << "unknown datatype";
      break;
    }

    mTime = pv->getMTime();

    emit valueUpdated(mTime);
}

/****************************************************************************/
